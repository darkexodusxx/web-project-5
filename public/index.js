document.addEventListener("DOMContentLoaded", function () {
    function checkInputValue(el) {
        if (/^[0-9]+$/.test(el.value)) {
            if (el.classList.contains("error")) {
                el.classList.remove("error");
            }
            return true;
        } else {
            if (!el.classList.contains("error")) {
                el.classList.add("error");
            }
            return false;
        }
    }
    function handleClick() {
        if (!checkInputValue(count) || !checkInputValue(price)) {
            return;
        }
        result.value = price.value * count.value;
    }
    const price = document.getElementById("price");
    const count = document.getElementById("count");
    let result = document.getElementById("result");
    const button = document.getElementById("button");
    button.addEventListener("click", handleClick);
});

